var model = require('../lib/cql')('reactors');
var logger = require('../lib/logger');

module.exports = {
    
    getReactors : function(callback) {

        var options = {};
        options.fields = ['id', 'name'];

        model.select(options, callback);

    },
    
    getReactor : function(id, callback) {
        
        var options = {};
        options.condition = {id:id};
        
        model.select(options, callback);
        
    },
    
    insertReactor: function(fields, callback){
        model.insert(fields, callback);
    },
    
    removeReactor: function(id, callback){
        model.remove({id:id}, callback);
    }
};


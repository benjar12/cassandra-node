var uuid = require('node-uuid');
var r = require("../dao/reactors");

describe("reactors data access object", function() {

    var id = uuid.v4();

    it("should insert into the database", function() {

        var data;
        var err;
        var done = false;

        var fields = {
            id : id,
            name : 'unittest'
        };

        r.insertReactor(fields, function(e, d) {
            err = e;
            data = d;
            done = true;
        });

        // wait for async call to be finished
        waitsFor(function() {
            return done;
        }, 'should insert into the database', 1000);

        // run the assertion after callback
        runs(function() {
            expect(err).toEqual(undefined);
        });

    });

    it("should not have an error", function() {

        var data;
        var err;

        r.getReactors(function(e, d) {
            err = e;
            data = d;
        });

        // wait for async call to be finished
        waitsFor(function() {
            return (data !== undefined || err !== undefined);
        }, 'should not have an error', 1000);

        // run the assertion after callback
        runs(function() {
            expect(err).toEqual(undefined);
            expect(data.rows.length).toBeGreaterThan(0);
        });

    });
    
    it("should return the record we just inserted", function() {

        var data;
        var err;

        r.getReactor(id, function(e, d) {
            err = e;
            data = d;
        });

        // wait for async call to be finished
        waitsFor(function() {
            return (data !== undefined || err !== undefined);
        }, 'should not have an error', 1000);

        // run the assertion after callback
        runs(function() {
            expect(err).toEqual(undefined);
            expect(data.rows.length).toBeGreaterThan(0);
        });

    });
    
    it("should delete the record we just inserted", function() {

        var data;
        var err;

        r.removeReactor(id, function(e, d) {
            err = e;
            data = d;
        });

        // wait for async call to be finished
        waitsFor(function() {
            return (data !== undefined || err !== undefined);
        }, 'should not have an error', 1000);

        // run the assertion after callback
        runs(function() {
            expect(err).toEqual(undefined);
        });

    });

});

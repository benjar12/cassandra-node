var logger = require('./lib/logger'),
    express = require('express'),
    app = express(),
    bodyParser = require('body-parser');

// parse various different custom JSON types as JSON
app.use(bodyParser.json({ type: 'application/*+json' }));

// parse some custom thing into a Buffer
app.use(bodyParser.raw({ type: 'application/vnd.custom-type' }));

// parse an HTML body into a string
app.use(bodyParser.text({ type: 'text/html' }));

//register heartbeat url
app.get('/heartbeat', function(req, res){
    res.send({succes:true});
});

//put database connections and such in an object to pass to the routes
var deps = {
    reactors: require('./dao/reactors')
};

// create application/json parser
var jsonParser = bodyParser.json();

//register route files. Could be moved to a router
var reactor = require('./app/reactor')(deps);
app.get('/reactor', reactor.get);
app.post('/reactor', jsonParser, reactor.post);

var server = app.listen(3000, function () {

    var host = server.address().address;
    var port = server.address().port;

    logger.info('App listening at http://%s:%s', host, port);

});
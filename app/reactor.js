var uuid = require('node-uuid');

module.exports = function(deps){
    var dao = deps.reactors;
    
    return {
        //find one
        get: function(req, res){
            var id = req.query.id;
            var response = {success:true, status:200};
            
            if(!id){
                
                response.status = 400;
                response.error = "Missing id!";
                res.status(response.status).jsonp(response);
                
            }else{
                
                dao.getReactor(id, cb);
                
                function cb(error, data){
                    
                    if(error){
                        response.error = error;
                        response.status = 500;
                    }
                    
                    if(data){
                        
                        if(data.rows){response.result = data.rows;}
                        
                    }
                    
                    res.status(response.status).jsonp(response);
                    
                }
                
            }
            
        },
        //create
        post: function(req, res){
            
            var data = req.body;
            var response = {success:true, status:200};
            
            //set possiable params
            var params = {
                id: uuid.v4(),
                architect: null,
                capacity_2008: null,
                capacity_2009: null,
                capacity_2010: null,
                capacity_2011: null,
                capacity_2012: null,
                capacity_2013: null,
                commercial_operation_date: null,
                contructor: null,
                docket_number: null,
                licensed_mwt: null,
                licensee: null,
                link: null,
                location: null,
                name: "",
                notes: null,
                nrc_region: null,
                operating_license_date: null,
                operating_license_expires: null,
                permit_date: null,
                reactor_type: null,
                renewed_operating_license_issued: null,
                supplier: null
            };
            
            //check body for possiable params and change from null
            for(var key in params) { params[key] = (data[key] || params[key]); }
            
            console.log(req);
            
            //validate 
            if(params.name.length < 2){
                
                response.status = 400;
                response.error = "Missing name!";
                res.status(response.status).jsonp(response);
                
            }else{
                
                dao.insertReactor(params, cb);
                
                function cb(error, data){
                    
                    if(error){
                        response.error = error;
                        response.status = 500;
                    }
                    
                    if(data){
                        
                        if(data){response.result = data;}
                        
                    }
                    
                    res.status(response.status).jsonp(response);
                    
                }
                
            }
            
        }
    };
};

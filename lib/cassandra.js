/*
 * We pull this out into a seperate file so when
 * ready to configure its one place to change the host and such.
 */
var client;

if(!client){
    var logger = require('./logger');
    var cassandra = require('cassandra-driver');
    client = new cassandra.Client({contactPoints:['localhost'], keyspace:'demo'});
    logger.info('New cassandra client registerd.');
}
module.exports = client;
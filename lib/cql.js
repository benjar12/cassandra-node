var client = require('./cassandra');
var logger = require('./logger');
var jsonSql = require('json-sql')();

module.exports = function(tablename) {

    return {
        select : function(options, callback) {

            var values = [];
            var fields = ['*'];
            var condition = {};
            
            if(options){
                if(options.fields) fields = options.fields;
                if(options.condition) condition = options.condition;
            }
            
            var sql = jsonSql.build({
                type: 'select',
                table: tablename,
                fields: fields,
                condition: condition
            });            

            //execute
            executeQuery(sql, callback);

        },

        insert : function(fields, callback) {

            var sql = jsonSql.build({
                type : 'insert',
                table : tablename,
                values : fields
            });
            
            executeQuery(sql, callback);

        },
        
        remove : function(condition, callback){
            
            var sql = jsonSql.build({
                type: 'remove',
                table: tablename,
                condition: condition
            });
            
            executeQuery(sql, callback);
            
        }
    };

};

function executeQuery(sql, callback) {
    
    var params = [];
    var query = sql.query;
    var distParams = sql.values;
    
    //we need to turn the dict into an array
    for(var key in distParams) { params.push(distParams[key]); }
    
    //this lib was for ms sql,
    //so we need to change placeholders to question marks
    query = query.replace(/(\$p\d{1,2})/g, "?");

    //log sql for debugging
    logger.info('Executing cql: ', query, ' With Params:', params);

    client.execute(query, params, cb);

    function cb(err, data) {

        //log any errors
        if (err)
            logger.error(err);

        //if callback return data
        (callback) ? callback(err, data) : false;

    }

}
